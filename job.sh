#!/bin/bash

# CABECALHO ===========================================

#SBATCH -J job_teste
#SBATCH -o saida_%j
#SBATCH --output=output_%j.o
#SBATCH --error=error_%j.o
#SBATCH -n 41
#SBATCH -t 01:00:00
#SBATCH --mem=10000
#SBATCH --mail-user=otavio.pinheiro@estudante.ufscar.br
#SBATCH --mail-type=ALL
#SBATCH --account=u789053

# CORPO ==============================================

# Nome do perfil do rclone
service="cloud"

# Pasta de input no drive
remote_in="Cluster/Entrada"

# Pasta de output no drive
remote_out="Cluster/Saida"

# Pasta com o container
remote_sing="Cluster/Containers"

# Pasta de input no container
container_in="/opt/input" # O programa deve ler dessa pasta

# Pasta de output no container
container_out="/opt/output" # O programa deve escrever nessa pasta

# Pasta onde o container sera salvo no cluster
local_sing="." # Local onde o job foi executado

#  Pasta temporaria no cluster
local_job="/scratch/job.${SLURM_JOB_ID}"

# Pasta de entrada no cluster
local_in="${local_job}/input/"

# Pasta de saida no cluster
local_out="${local_job}/output/"

# TRAP ==================================================

function limpar(){
	echo "Limpando a pasta local_job..."
	rm -rf "${local_job}"
}
trap limpar EXIT HUP INT TERM ERR # Define a funcao limpar como uma trap

set -eE # Para a trap funcionar quando der erro

# FUNCAO DE ERRO ========================================
function sendErr(){ # Envia uma mensagem de erro, a saida de erro e a saida padrao para o telegram
  echo "Erro!!!!:("
  sendMsgDiscord "Erro no Job_${SLURM_JOB_ID}!"
  sendFileDiscord "Error.e"
  sendFileDiscord "Output.o"
  cleanJob
}
trap ERR

# CONFIGURACAO DAS PERMISSOES ===========================

umask 077 # Remove as permissoes de escrita, leitura e execucao dos outros usuarios

# COPIA DO CONTAINER ====================================

sing="Singularity_ompi.simg" # Nome do container no drive

echo "Copiando o container..."

rclone copyto "${service}:${remote_sing}/${sing}" "${local_sing}/Singularity.simg" # Copia o container

# CRIACAO DE PASTAS TEMPORARIAS =========================

echo "Criando pastas temporarias..."

mkdir -p "${local_in}"
mkdir -p "${local_out}"

# COPIA DA INPUT ========================================

echo "Copiando input..."

rclone copy "${service}:${remote_in}/" "${local_in}/" # Copia os arquivos na pasta de entrada do drive

# FUNCOES BOT ===========================================

echo "Definindo Variaveis dos Bots"

# Variaveis telegram =============================================
bot_key="" # bot
chat_id="" # chat onde o bot ira enviar as mensagens

# Variaveis discord ======================================================================================================================================
discord_webhook="https://discord.com/api/webhooks/782414784439255101/DqxQ1xO1OJQC4t7lXj6irl_VgmwnMYuNPcQvQRa5gV0A40Vaz3lXH5PvHlzzCsHfh0ZQ" # webhook obtid$

echo "Definindo Funcoes dos Bots"

# Funcoes Telegram =======================================================================================

echo "Telegram"

function sendPhotoTelegram(){
 #curl https://api.telegram.org/$bot_key/sendphoto -F "chat_id=$chat_id" -F "photo=@$1"
  curl -F "photo=@$1" https://api.telegram.org/$bot_key/sendphoto?chat_id=$chat_id
}

function sendFileTelegram(){
  curl -F "document=@$1" https://api.telegram.org/$bot_key/sendDocument?chat_id=$chat_id
}

function sendMsgTelegram(){
  curl -X POST -H 'Content-Type: application/json' -d "{\"chat_id\": \"$chat_id\", \"text\": \"$1\", \"disable_notification\": true}"  "https://api.telegram.org/$bot_key/sendMessage"
}

echo "Discord"

# Funcoes Discord =========================================================================================
function sendFileDiscord(){
        curl -i -H 'Expect: application/json' -F file=@$1 -F 'payload_json={ "wait": true}' $discord_webhook
}

function sendMsgDiscord(){
	curl -X POST -H "Content-Type: application/json" -d "{\"content\": \"$1\"}" $discord_webhook
}


# EXECUCAO DO PROGRAMA ===================================

echo "Executando job_${SLURM_JOB_ID}..."

#sendMsgDiscord "Executando job_${SLURM_JOB_ID}..."
#sendMsgTelegram "Executando job_${SLURM_JOB_ID}..."

mpirun -n 20 singularity exec Singularity.simg /opt/multiplica $(cat ${local_in}/Config_Matriz.txt)\
      --bind=/scratch:/scratch \
      --bind=/var/spool/slurm:/var/spool/slurm \
      --bind="${local_in}:${container_in}" \
      --bind="${local_out}:${container_out}" 
      

echo "Encerrando..."
     


#sendMsgDiscord "Finalizado job_${SLURM_JOB_ID}!"
#sendFileDiscord "output_${SLURM_JOB_ID}.o"
#sendFileDiscord "imagem.jpeg"

#sendMsgTelegram "Finalizado job_${SLURM_JOB_ID}!"
#sendFileTelegram "output_${SLURM_JOB_ID}.o"
#sendPhotoTelegram "imagem.jpeg"

# ENVIO DOS ARQUIVOS DE SAIDA ============================
# criar um novo job para isso caso tenham muitos arquivos

echo "Enviando arquivos de saida..."

rclone move "${local_out}" "${service}:${remote_out}" # Envio dos arquivos na pasta de saida do cluster
rclone copy "output_${SLURM_JOB_ID}.o" "${service}:${remote_out}" # Envio do arquivo definido como saida padrao do programa
