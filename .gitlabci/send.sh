echo "Configurando ambiente..."
if [[ -z $COLLECTION_CONTAINER ]]; then
  COLLECTION_CONTAINER=collection/container
fi
RCLONE_FILE=~/.config/rclone/rclone.conf
mkdir -p "$(dirname "${RCLONE_FILE}")"

echo "Configurando rclone..."
echo "${RCLONE_CONF}" | base64 -d  >> "${RCLONE_FILE}"

echo "Enviando arquivos..."
NOW=$(date +'%Y%m%d%H%M%S')
for filename; do
  if [[ -f $filename ]]; then
    path="$(dirname "${filename}")"
    filename="$(basename "${filename}")"
    if [[ "$filename" == *.* ]]; then
      dest="${filename%.*}.${filename##*.}"
    else
      dest="${filename}"
    fi
    rclone copyto "${path}/${filename}" "cloud:${COLLECTION_CONTAINER}/${dest}"
  fi
done
rclone sync --auto-confirm "Configuracoes" "cloud:${PROG_CONFIG}"
